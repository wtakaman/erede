<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

use \The8co\eRede\Acquirer;
use \The8co\eRede\Model\CancelRequest;
use \The8co\eRede\Model\EnvironmentType;


$reqCancel = new CancelRequest();
$reqCancel->setTid("a");
$reqCancel->setNsu("a");
$reqCancel->setAuthorizationNumber("a");
$reqCancel->setDate("a");

/** @noinspection PhpParamsInspection */
$c = new Acquirer('123','123', EnvironmentType::Develop);
$v = $c->cancel($reqCancel);
echo $v->getMessage();