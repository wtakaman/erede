<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

use \The8co\eRede\Acquirer;
use \The8co\eRede\Model\FindRequest;
use \The8co\eRede\Model\EnvironmentType;

$reqFind = new FindRequest();
$reqFind->setTid("a");
$reqFind->Reference("a");

/** @noinspection PhpParamsInspection */
$c = new Acquirer('123','123', EnvironmentType::Develop);
$v = $c->find($reqFind);
echo $v->getMessage();

