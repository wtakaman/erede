<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
use \The8co\eRede\Acquirer;
use \The8co\eRede\Model\CaptureRequest;
use \The8co\eRede\Model\EnvironmentType;

$reqCap = new CaptureRequest();
$reqCap->setAmount(10.00);
$reqCap->setTid("a");

/** @noinspection PhpParamsInspection */
$c = new Acquirer('123','123', EnvironmentType::Develop);
$v = $c->capture($reqCap);
echo $v->getMessage();

