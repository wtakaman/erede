<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

use \The8co\eRede\Acquirer;
use \The8co\eRede\Model\AuthorizedCreditRequest;
use \The8co\eRede\Model\EnvironmentType;

$reqCredit = new AuthorizedCreditRequest();
$reqCredit->setAmount(0.51);
$reqCredit->setReference("123AA1");
$reqCredit->setCardNumber("123");
$reqCredit->setSecurityCode("479");
$reqCredit->setExpirationMonth(5);
$reqCredit->setExpirationYear(18);
$reqCredit->setCardHolderName("portador");
$reqCredit->setSubscription("1");
$reqCredit->setSoftDescriptor("abc");
$reqCredit->setPostalCode("");
$reqCredit->setAddressComplement("");
$reqCredit->setDocument("");
$reqCredit->setAddressStreet("");
$reqCredit->setAddressNumber("");
$reqCredit->setOrigin("01");
$reqCredit->setCapture(FALSE);
$reqCredit->setInstallments(0);


/** @noinspection PhpParamsInspection */
$c = new Acquirer('66415616','1233454', EnvironmentType::Develop);
$v = $c->authorizeCredit($reqCredit);
echo $v->getMessage();
echo $v->getTid();
