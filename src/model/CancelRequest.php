<?php
namespace The8co\eRede\Model;

/**
* Class CancelRequest
* 
* This class is filled with information to cancel a transaction.
* The request object sent to the server.
*/
class CancelRequest extends BaseModel
{
    private $tid = "";
    private $nsu = "";
    private $date = "";
    private $amount = "";
    private $callbackUrl = "";

    public function getTid(){
        return $this->tid;
    }

    public function setTid($tid){
        $this->tid = $tid;
    }

    public function getNsu(){
        return $this->nsu;
    }

    public function setNsu($nsu){
        $this->nsu = $nsu;
    }

    public function getDate(){
        return $this->date;
    }

    public function setDate($date){
        $this->date = $date;
    }

    public function getCallbackUrl()
    {
        return $this->callbackUrl;
    }

    public function setCallbackUrl($callbackUrl)
    {
        $this->callbackUrl = $callbackUrl;
    }

    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    public function getAmount()
    {
        return $this->amount;
    }

   /**
   * Maps sdk's request object to public Rede's Wcf request object.
   *
   * @param CancelRequest $cancelRequest
   * @param Security $security
   * @return array
   */
    public static function map($cancelRequest, $security){
        $wcfRequest = array(
            "auth"          => array($security->affiliation, $security->password, 'basic' ),
            "form_params"   => array(
                "tid"       => self::toNull($cancelRequest->getTid())             ,
                "nsu"       => self::toNull($cancelRequest->getNsu())             ,
                "date"      => self::toNull($cancelRequest->getDate())            ,
                "amount"    => self::toNull($cancelRequest->getAmount())          ,
                "callbackUrl"    => self::toNull($cancelRequest->getCallbackUrl()),
            )
        );
        return $wcfRequest;
    }
}
