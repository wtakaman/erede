<?php
namespace The8co\eRede\Model;

/**
* Class CancelResponse
* 
* This class is returned after cancel method.
*/
class CancelResponse extends BaseModel
{
    private $returnCode = 0;
    private $message = "";

    public function getReturnCode(){
        return $this->returnCode;
    }

    public function setReturnCode($returnCode){
        $this->returnCode = $returnCode;
    }

    public function getMessage(){
        return $this->message;
    }

    public function setMessage($message){
        $this->message = $message;
    }

   /**
   * Maps the public Rede's Wcf response object to sdk's response object.
   *
   * @param array $wcfResponse
   * @return CancelResponse
   */
    public static function map($wcfResponse){
        $cancelResponse = new CancelResponse();

        if($wcfResponse->returnCode == "success")
            $cancelResponse->setReturnCode(TransactionStatus::Success);
        else
            $cancelResponse->setReturnCode(TransactionStatus::TransactionNotProcessed);

        $cancelResponse->setMessage($wcfResponse->returnMessage);

        return $cancelResponse;
    }
}