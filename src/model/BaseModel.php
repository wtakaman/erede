<?php
namespace The8co\eRede\Model;

/**
* Class BaseModel
* 
* This class is used to help conversion methods.
*/
class BaseModel
{
    static protected function toNull($value)
    {
        if ($value === '')
            return NULL;
        else
            return $value;
    }
}