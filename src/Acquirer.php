<?php

namespace The8co\eRede;
use \The8co\eRede\Model\AuthorizedCreditResponse;
use \The8co\eRede\Model\Security;
use \The8co\eRede\Model\TransactionStatus;
use \The8co\eRede\Model\CancelResponse;
use \The8co\eRede\Model\AuthorizedCreditRequest;
use \The8co\eRede\Model\FindResponse;
use \The8co\eRede\Model\EnvironmentType;
use \The8co\eRede\Model\CaptureRequest;
use \The8co\eRede\Model\CaptureResponse;
use \The8co\eRede\Model\CancelRequest;
use \The8co\eRede\Model\FindRequest;


/**
* Class Acquirer
*
* Main class of SDK. Contain all methods to authorize, capture or cancel a transaction.
*/
class Acquirer {
    /**
    * Object that holds affiliation's information.
    * @var Security
    */
    private $security = NULL;

    /**
    * returns the wcf url of the selected environment.
    *
    * @return string
    */

    // Testes:
    // Produção: https://ecommerce.userede.com.br/api/v2/refunds
    private function cancel_path() {
        if ($this->security->environment == EnvironmentType::Homolog)
            return "https://scommerce.userede.com.br/api/v2/refunds";
        elseif($this->security->environment == EnvironmentType::Production)
            return "https://ecommerce.userede.com.br/api/v2/refunds";
        else
            return "";
    }

    private function wsdl_path() {
        if ($this->security->environment == EnvironmentType::Homolog)
            return "https://scommerce.userede.com.br/Redecard.Komerci.External.WcfKomerci/KomerciWcfV2.svc?WSDL"; //return "https://scommerce.userede.com.br/Redecard.Komerci.External.WcfKomerci/KomerciWcf.svc?WSDL";
        elseif($this->security->environment == EnvironmentType::Production)
            return "https://ecommerce.userede.com.br/Redecard.Adquirencia.Wcf/KomerciWcfV2.svc?WSDL"; // "https://ecommerce.userede.com.br/Redecard.Adquirencia.Wcf/KomerciWcf.svc?WSDL";
        else
            return "http://localhost:50000/KomerciWcf.svc?WSDL";
    }

    /**
    * Default Constructor that initialize Security object.
    *
    * @param string $affiliation
    * @param string $password
    * @param EnvironmentType $environment
    */
    function __construct($affiliation, $password, $environment) {
        $this->security = new Security($affiliation, $password, $environment);
   }

   /**
   * Calls authorizedCredit method.
   *
   * @param AuthorizedCreditRequest $request
   * @return AuthorizedCreditResponse
   */
   function authorizeCredit($request){
        try {
            // create wcf objects.
            $client = new \SoapClient($this->wsdl_path());
            $wcfRequest = AuthorizedCreditRequest::map($request, $this->security);

            // make the request.
            $result = $client->GetAuthorizedCredit(array('request' => $wcfRequest));
            // mapping the result.
            $response = AuthorizedCreditResponse::map($result->GetAuthorizedCreditResult);

        } catch (\Exception $e) {
            $response = new AuthorizedCreditResponse();
            $response->setReturnCode(TransactionStatus::TransactionNotProcessed);
            $response->setMessage($e->getMessage());
        }

        return $response;
   }

   /**
   * Captures the transaction.
   *
   * @param CaptureRequest $request
   * @return CaptureResponse
   */
   function capture($request){
        try {
            // create wcf objects.
            $client = new \SoapClient($this->wsdl_path());
            $wcfRequest = CaptureRequest::map($request, $this->security);

            // make the request.
            $result = $client->ConfirmTxnTID(array('request' => $wcfRequest));

            // mapping the result.
            $response = CaptureResponse::map($result->ConfirmTxnTIDResult);
        } catch (\Exception $e) {
            $response = new CaptureResponse();
            $response->setReturnCode(TransactionStatus::TransactionNotProcessed);
            $response->setMessage($e->getMessage());
        }

        return $response;
   }

   /**
   * Calls find method.
   *
   * @param FindRequest $request
   * @return FindResponse
   */
   function find($request){
        try {
            // create wcf objects.
            $client = new \SoapClient($this->wsdl_path());
            $wcfRequest = FindRequest::map($request, $this->security);
            
            // make the request.
            $result = $client->Query(array('request' => $wcfRequest));

            // mapping the result.
            $response =FindResponse::map($result->QueryResult);
        } catch (\Exception $e) {
            $response = new FindResponse();
            $response->setReturnCode(TransactionStatus::TransactionNotProcessed);
            $response->setMessage($e->getMessage());
        }

        return $response;
   }
   
   /**
   * Calls cancel method.
   *
   * @param CancelRequest $request
   * @return CancelResponse
   */
   function cancel($request){
        try {
            // create wcf objects.
            $client = new \GuzzleHttp\Client();
            $res = $client->request('POST', $this->cancel_path(), CancelRequest::map($request, $this->security));
            $jsonContent = json_decode($res->getBody());
            $response = CancelResponse::map($jsonContent);
        } catch (\GuzzleHttp\Exception\RequestException $e) {
            $response = new CancelResponse();
            $response->setReturnCode(TransactionStatus::TransactionNotProcessed);
            $jsonContent = json_decode($e->getResponse()->getBody());
            $response->setMessage($jsonContent->errors[0]->Code . ':' . $jsonContent->errors[0]->Message);
        }
        return $response;
   }

}